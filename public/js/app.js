
var app = angular.module("EventBuzz", ["ngRoute", "ngMap", "ngSanitize","ui.bootstrap"]);

app.config(function($routeProvider, $httpProvider) {
    $routeProvider
        .when('/event/:eventID/details', {
            templateUrl: 'views/event/eventDetails.html',
            controller: 'EventDetailsCtrl'
        })
        .when('/home', {
            templateUrl: 'views/home/home.html',
            controller: 'HomeCtrl'
        })
        .when('/user', {
            templateUrl: 'views/user/user.html',
            controller: 'UserCtrl'
        })
         .when('/user/:userId', {
             templateUrl: 'views/user/userDetails.html',
             controller: 'UserDetailsCtrl'
         })
        .when('/profile', {
            templateUrl: 'views/profile/profile.html',
            controller: 'ProfileCtrl',
            resolve: {
                loggedin: checkLoggedin
            }
        })
        .when('/login', {
            templateUrl: 'views/login/login.html',
            controller: 'LoginCtrl'
        })
        .when('/register', {
            templateUrl: 'views/register/register.html',
            controller: 'RegisterCtrl'
        })
        .otherwise({
            redirectTo: '/home'
        });
});

var checkLoggedin = function($q, $timeout, $http, $location, $rootScope)
{
    var deferred = $q.defer();

    $http.get('/loggedin').success(function(user)
    {
        $rootScope.errorMessage = null;
        // User is Authenticated
        if (user !== '0')
        {
            $rootScope.currentUser = user;
            deferred.resolve();
        }
        // User is Not Authenticated
        else
        {
            $rootScope.errorMessage = 'You need to log in.';
            deferred.reject();
            $location.url('/login');
        }
    });
    
    return deferred.promise;
};


app.controller('EventDetailsCtrl', function ($scope, $http, $routeParams, $rootScope) {
    var id = $routeParams.eventID;
    console.log("EventID:" + id);
    console.log($routeParams);
    console.log("rootscope ");
    console.log($rootScope);
    var url = "http://api.eventful.com/json/events/get?callback=?&app_key=X7bncMD2GPV2svfN&id=" + id;
    $.getJSON(url, function (result) {
        $scope.event = result;
        $scope.$apply();
        //console.log($scope.event);
        $rootScope.isDetailsView = true;
        $scope.eventID = id;
        // alert($rootScope.isDetailsView);
    })
    
    $scope.createEvent = function (event) {
        console.log("event " + event.title);
        console.log("event " + event.id);
        console.log("event " + $routeParams.eventID);
        console.log("event " + $rootScope.id);
        console.log("event " + $scope.eventID);
        $http.post("/rest/event/" + $scope.eventId + "/details", event)
            .success(function () {
                $scope.event = event;
            });
    }


   
});


