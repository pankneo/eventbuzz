app.factory("UserService", function($http){
    
    var findAllUsers = function(callback) {
        $http.get("/rest/user")
        .success(callback);
    };

    var findUserWithID = function (id,callback) {
        $http.get("/rest/user/"+id)
        .success(callback);
        console.log("ID in User Service:");
        console.log(id);
    };
    
    var deleteUser = function(id, callback) {
        $http.delete('/rest/user/'+id)
        .success(callback);
    };
    
    var updateUser = function(id, user, callback) {
        $http.put('/rest/user/'+id, user)
        .success(callback);
    };
    
    var createUser = function(user, callback) {
        $http.post('/rest/user', user)
        .success(callback);
    };
    var updateUserData = function (eventid, user, callback) {
        $http.put('/rest/event/' + eventid + '/details',user)
        .success(callback);
    };
    var getComments = function (eventid, callback) {
        $http.get('/rest/event/' + eventid + '/details')
        .success(callback);
    }

    var findWithAttr= function findWithAttr(array, attr, value) {
        for (var i = 0; i < array.length; i += 1) {
            if (array[i][attr] === value) {
                return i;
            }
        }
        return -1;
    }


  
    
    return {
        findAllUsers: findAllUsers,
        deleteUser: deleteUser,
        updateUser: updateUser,
        createUser: createUser,
        findUserWithID: findUserWithID,
        updateUserData: updateUserData,
        getComments: getComments,
        findWithAttr: findWithAttr,
    };
});
