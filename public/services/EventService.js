app.factory("EventService", function($http){
    
    //Find popular events by making corresponding API call.
    var findAllPopularEvents = function (callback) {
        $.getJSON("http://api.eventful.com/json/events/search?callback=?&location=Boston&app_key=X7bncMD2GPV2svfN&sort_order=popularity&")
    .success(callback);
    };

    //Find specific event by making corresponding API call
    var findSpecificEvent = function (searchtxt,callback) {
        var url = "http://api.eventful.com/json/events/search?callback=?&app_key=X7bncMD2GPV2svfN&keywords=" + searchtxt;
        $.getJSON(url).success(callback);
    };

    return {
        findSpecificEvent:findSpecificEvent,
        findAllPopularEvents: findAllPopularEvents,
    };
    
});