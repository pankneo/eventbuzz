app.controller("RegisterCtrl", function ($scope, $http, $location, $rootScope) {

    $(document).ready(function () {
        $('.validate-pass').hide();
    });
    $scope.register = function(user){
        if (user != null) {
            if (user.password != user.password2 || !user.password || !user.password2) {
                $rootScope.message = "Your passwords don't match";
                $('.validate-pass').show();
                $('.validate-pass').text("Passwords do not match");
            }
            else if ($('#checkTOS:checked').length === 0) {
                $('.validate-pass').text("You must agree with the Terms and Condtions")
                $('.validate-pass').show();
            }
            else {
                $http.post("/register", user)
                .success(function (response) {
                    console.log(response);
                    if (response != null) {
                        $rootScope.currentUser = response;
                        $location.url("/profile");

                    }
                });
            }
        }
    }
});
