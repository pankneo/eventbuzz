app.controller("NavCtrl", function($scope, $http, $location, $rootScope,EventService){
   $scope.logout = function(){
       $http.post("/logout")
       .success(function(){
           $rootScope.currentUser = null;
           $location.url("/login");
       });
   }
   $rootScope.reload = function ()
   {
       EventService.findAllPopularEvents( function (result) {
           $rootScope.upcomingEvents = result.events.event;
           $rootScope.$apply();
           document.getElementById("homehead").innerHTML = "Popular events around you!!!"
       });

   }
   $rootScope.search = function () {
       $rootScope.isSearched = true;
       $rootScope.upcomingEvents = null;
       $location.url("/home");
       EventService.findSpecificEvent($scope.searchtxt, function (result) {
           if (result.events != null) {
               $rootScope.upcomingEvents = result.events.event;
               $rootScope.$apply();
               document.getElementById("txtsearch").value = ""
               document.getElementById("homehead").innerText = "Searched Events:";
           }
           else {
               $('#homehead').html('<br><br><p class="alert alert-warning">Sorry!! Unable to find the matching event. Please try different search.<p>');
               $('.pagination').hide();
               $('#txtsearch').val('');
           }
       });
       
     
   };

});