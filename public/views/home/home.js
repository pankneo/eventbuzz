app.controller('HomeCtrl', function ($scope, $http, UserService, EventService, $rootScope) {

    if (!$rootScope.isSearched) {
        EventService.findAllPopularEvents(function (result) {
            if ($rootScope.isDetailsView) {
                $rootScope.isDetailsView = false;
            }
            else {
                $rootScope.upcomingEvents = result.events.event;
                $rootScope.$apply();
            }



        });

    }
    //Pagination Settings
    $scope.totalItems = 64;
    $scope.currentPage = 1;


    $scope.pageChanged = function () {
        $log.log('Page changed to: ' + $scope.currentPage);
    };

    $scope.maxSize = 5;
    $scope.bigTotalItems = 175;
    $scope.bigCurrentPage = 1;
    console.log("MaxSize:" + $rootScope.maxSize + " TI:" + $rootScope.bigTotalItems + " CurrentPage:" + $rootScope.bigCurrentPage);



});