app.controller('UserCtrl', function ($scope, $http, UserService, $rootScope) {
    var usrID = $rootScope.currentUser._id;

    UserService.findAllUsers( function (users)
    {
        $scope.users = users;
        
    });
    
   
});

app.controller('UserDetailsCtrl', function ($scope, $http, UserService, $routeParams, $rootScope, UserService, $location) {

    if ($rootScope.currentUser != null) {
        var usrID = $routeParams.userId;

        UserService.findUserWithID(usrID, function (user) {
            $scope.select = user[0];
            if ($scope.select._id === $rootScope.currentUser._id) {
                document.getElementById("btnFollow").style.visibility = "hidden";
            }

        });
    }
    else {
        $location.url("/login");
    }
        

    $scope.follow = function (user) {

        var currentFollowing = $rootScope.currentUser.follows;
        var userid = user._id;
        var username = user.username;
        var objFollow = { "userID": userid, "username": username };
        var index = UserService.findWithAttr($rootScope.currentUser.follows, 'userID', userid);
        if (index == -1 || index == undefined) {
            $rootScope.currentUser.follows.push(objFollow);

            UserService.updateUser($rootScope.currentUser._id, $rootScope.currentUser, function (user) {
                document.getElementById("alert").style.visibility = "visible";
            });
        }

    }

});