

app.controller('EventDetailsCtrl', function ($scope, $routeParams, $rootScope, UserService) {
    var id = $routeParams.eventID;
    var url = "http://api.eventful.com/json/events/get?callback=?&app_key=X7bncMD2GPV2svfN&id=" + id;
    $.getJSON(url, function (result) {
        $scope.event = result;
        $scope.$apply();
        console.log($scope.event);
        $rootScope.isDetailsView = true;
        $scope.eventID = id;


        if ($rootScope.currentUser != null) {
            var currentLikes = $rootScope.currentUser.likes;
            var eventid = $scope.event.id;
            var userid = $rootScope.currentUser._id;
            var objLike = { "eventID": $scope.event.id, "title": $scope.event.title };            
            var index = UserService.findWithAttr($rootScope.currentUser.likes, 'eventID', eventid);
            if (index == -1 || index == undefined) {
                //$rootScope.currentUser.likes.push(objLike);
            }
            else {
                document.getElementById("btnLike").innerHTML = " <span id='likeDecider' class='glyphicon glyphicon-thumbs-down'>Unlike</span>";
                $scope.unlike = true;
                $scope.likeIndex = index;
            }
        }
        else {
            //Disabling like button and comment section for unauthenticated users
            document.getElementById("btnLike").disabled = true;
            $("#commentSection :input").attr("disabled", true);
            

        }
        //Fetch comments
        UserService.getComments(id, function (comments) {
            console.log("Fetched Comments:");
            console.log(comments);
            $scope.fetchedComments = comments;
        });


    });


    $scope.addLikes = function (event) {
        var eventid = $scope.event.id;
        var likeDecider = document.getElementById("likeDecider").innerText;
        var index = UserService.findWithAttr($rootScope.currentUser.likes, 'eventID', eventid);
        if (likeDecider.toUpperCase().indexOf("UNLIKE") > -1) {
            $rootScope.currentUser.likes.splice(index, 1);
            document.getElementById("btnLike").innerHTML = " <span id='likeDecider' class='glyphicon glyphicon-thumbs-up'>Like</span>";

        }
        else {
            //Creating like object
            var objLike = { "eventID": $scope.event.id, "title": $scope.event.title };

            if (index == -1 || index == undefined) {
                $rootScope.currentUser.likes.push(objLike);
            }
            document.getElementById("btnLike").innerHTML = " <span id='likeDecider' class='glyphicon glyphicon-thumbs-down'>Unlike</span>";
        }
        UserService.updateUserData(eventid, $rootScope.currentUser, function (user) {
        });
    };


    $scope.addComment = function (comment) {
        var eventid = $scope.event.id;
        var userid = $rootScope.currentUser._id;
        var username = $rootScope.currentUser.username;
        console.log(comment);
        var commentObj = { "eventID": eventid, userID: userid, "comment": comment,"username":username };
        $rootScope.currentUser.comments.push(commentObj);
         console.log($rootScope.currentUser);
        UserService.updateUserData(id, $rootScope.currentUser, function (user) {
            $scope.comment = "";
            UserService.getComments(id, function (comments) {
                $scope.fetchedComments = comments;
            });
        });

        



    }


});



