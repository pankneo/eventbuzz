app.controller('ProfileCtrl', function($scope, $http, UserService) {
   
    
   
    
    UserService.findAllUsers(function(users)
    {
        $scope.users = users;
    });
    
    $scope.remove = function(user)
    {
        UserService.deleteUser(user._id, function(users){
           $scope.users = users; 
        });
    }
    
    $scope.update = function(user)
    {
        UserService.updateUser(user._id, user, function(users){
            $scope.users = users;
            document.getElementById("alert").style.visibility = "visible";
        });
    }
    
    $scope.add = function(user)
    {
        UserService.createUser(user, function(users){
            $scope.users = users; 
        });
    }
    
    $scope.select = function(user)
    {
        $scope.user = user;
    }
});