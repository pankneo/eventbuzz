app.controller("LoginCtrl", function ($scope, $http, $location, $rootScope) {
    $(document).ready(function () {
        $('.validate').hide();
    });
    $scope.login = function(user){
        $http.post("/login", user)
            .error(function (ex) {
                $('.validate').show();
            })
        .success(function (response) {
            $('.validate').hide();
            $rootScope.currentUser = response;
            $location.url("/home");
        });
    }
});
