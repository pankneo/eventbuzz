var mongoose = require("mongoose");

module.exports = function()
{
    //LikeSchema keeps the mapping of like for an event
    var LikeSchema = mongoose.Schema(
        {
            eventID: String,
            title: String
        })

    //CommentSchema keeps the mapping of comment on an event
    var CommentSchema = mongoose.Schema(
        {
            eventID: String,
            userID:String,
            comment: String,
            username:String,
            dateAdded: { type: Date, default: Date.now }
        })

    //FollowedUsers keeps the track of users being followed
    var FollowedUsers = mongoose.Schema(
        {
            userID: String,
            username:String
        })

    //UserSchema is the master schema that revolves around user perspective
    var UserSchema = new mongoose.Schema({
        username:    String,
        password:    String,
        firstName:   String,
        lastName:    String,
        email:       String,
        roles:      [String],
        likes:      [LikeSchema],
        comments: [CommentSchema],
        follows: [FollowedUsers]
    });


    var UserModel = mongoose.model('UserModel', UserSchema);
    

    return UserModel;
};
