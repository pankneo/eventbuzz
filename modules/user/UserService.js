module.exports = function(UserModel, app, auth)
{
    // Retrieve all users
    app.get("/rest/user", auth, function(req, res)
    {
        var id = req.user._id;
        UserModel.find({ _id: { '$ne': id } }, function (err, users)
       {
         res.json(users);
       });
    });


    //Retrive user with specific Id
    app.get("/rest/user/:id", function (req, res) {
        UserModel.find({ "_id": req.params.id }, function (err, user) {
            res.json(user);
        });
    });


    // Remove existing user (Part of admin module --To do--)
    app.delete("/rest/user/:id", auth, function(req, res){
       UserModel.findById(req.params.id, function(err, user){
          user.remove(function(err, count){
            UserModel.find(function(err, users){
               res.json(users);
            });
          });
       });
    });
    

    // Modify existing user
    app.put("/rest/user/:id", auth, function(req, res){
        delete req.body._id;
        UserModel.findById(req.params.id, function(err, user){
           user.update(req.body, function(err, count){
             UserModel.find(function(err, users){
                res.json(users);
             });
           });
        });
    });
    
    // Creation of new user
    app.post("/rest/user", auth, function(req, res){
       UserModel.findOne({username: req.body.username}, function(err, user) {
          if(user == null)
          {
            user = new UserModel(req.body);
            user.save(function(err, user){
               UserModel.find(function(err, users){
                 res.json(users);
               });
            });
          }
          else
          {
            UserModel.find(function(err, users){
               res.json(users);
            });
          }
       });
    });




    


    app.put("/rest/event/:eventId/details", auth, function (req, res) {
        console.log("User add like hit");
        var user = req.body;       
        console.log("User Object");
        console.log(user);
        var id = user._id;
        console.log("User ID:"+id);
        UserModel.findById(id, function (err, user) {
            user.update(req.body, function (err, count) {
                UserModel.findById(id, function (err, user) {
                    console.log("commit success");
                    res.json(user);
                });
            });
        });
    });


    app.get("/rest/event/:eventId/details", function (req, res) {
        var evID=req.params.eventId;
        //UserModel.find(function(err,users){
        //    res.json(users);
        //})

        //UserModel.find({comments:eventID}, function (err, docs) {
        //    res.json(users);
        //});


        //.where('comments.eventID').equals(eventID);
        var query = UserModel.find({}).select('comments')
        //console.log(query);
        query.exec(function (err, usrArr) {
            if (err) return next(err);
            var commentsArray=[];

            var tlArraylength = usrArr.length;

            console.log(tlArraylength);
            for (i = 0; i < tlArraylength; i++) {
                //console.log(usrArr[i].comments);
                for (j = 0; j < usrArr[i].comments.length; j++) {

                    if (usrArr[i].comments[j].eventID === evID) {
                       commentsArray.push(usrArr[i].comments[j]);                       
                    }
                }
            }
            res.send(commentsArray);
            console.log(commentsArray);
        });

    })




}


    