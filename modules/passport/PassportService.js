var LocalStrategy = require('passport-local').Strategy;

module.exports = function(UserModel, passport, app)
{
    passport.use(new LocalStrategy(
    function(username, password, done)
    {
        UserModel.findOne({username: username, password: password}, function(err, user)
        {
            if (err) { return done(err); }
            if (!user) { return done(null, false); }
            return done(null, user);
        })
    }));
  
    passport.serializeUser(function(user, done) {
        done(null, user);
    });
    
    passport.deserializeUser(function(user, done) {
        done(null, user);
    });
    
    app.post("/login", passport.authenticate('local'), function(req, res){
        var user = req.user;
        res.json(user);
    });

    app.get('/loggedin', function(req, res)
    {
        res.send(req.isAuthenticated() ? req.user : '0');
    });
        
    app.post('/logout', function(req, res)
    {
        req.logOut();
        res.send(200);
    });     

    app.post('/register', function(req, res)
    {
        var newUser = req.body;
        newUser.roles = ['student'];
        UserModel.findOne({username: newUser.username}, function(err, user)
        {
            if(err) { return next(err); }
            if(user)
            {
                res.json(null);
                return;
            }
            var newUser = new UserModel(req.body);
            newUser.save(function(err, user)
            {
                req.login(user, function(err)
                {
                    if(err) { return next(err); }
                    res.json(user);
                });
            });
        });
    });
}