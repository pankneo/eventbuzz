/// <reference path="modules/passport/PassportService.js" />
var ipaddress = process.env.OPENSHIFT_NODEJS_IP   || '127.0.0.1';
var port      = process.env.OPENSHIFT_NODEJS_PORT || 3000;
var connectionString = process.env.OPENSHIFT_MONGODB_DB_URL || 'mongodb://localhost/test';

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var multer        = require('multer'); 
var passport      = require('passport');
var cookieParser  = require('cookie-parser');
var session       = require('express-session');
var mongoose      = require('mongoose');
var db = mongoose.connect(connectionString);

var EventSchema = mongoose.Schema(
   {
       eventId: String,
       title: String
   });
var EventModel = mongoose.model('EventModel', EventSchema);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(multer());
app.use(session({ secret: 'this is the secret' }));
app.use(cookieParser())
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(__dirname + '/public'));

var auth = function(req, res, next)
{
    if (!req.isAuthenticated())
        res.send(401);
    else
        next();
};

var UserModel = require('./modules/user/UserModel.js')();
var UserService = require('./modules/user/UserService.js')(UserModel, app, auth);

var PassportService = require('./modules/passport/PassportService.js')(UserModel, passport, app, auth);

app.listen(port, ipaddress);
